package com.enuri.shopmtc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopmtcApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShopmtcApplication.class, args);
	}

}
