package com.enuri.shopmtc.controller;

import com.enuri.shopmtc.model.CountryModel;
import com.enuri.shopmtc.model.SalaryModel;
import com.enuri.shopmtc.service.MasterDataBaseService;
import com.enuri.shopmtc.service.Slave1DataBaseService;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;

@Controller
public class HomeController {
    @Autowired
    private MasterDataBaseService masterDataBaseService;

    @Autowired
    private Slave1DataBaseService slave1DataBaseService;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView goHome(HttpServletRequest request) throws Exception{

        ModelAndView mav = new ModelAndView();

        List<SalaryModel> salaryList = masterDataBaseService.getSalary();
        //List<CountryModel> countryList = slave1DataBaseService.getCountry();
        List<CountryModel> countryList = slave1DataBaseService.getCrwlReg();

        mav.addObject("salaryList", salaryList);
        mav.addObject("countryList", countryList);
        mav.setViewName("content/home.html");

        return mav;
    }

}
