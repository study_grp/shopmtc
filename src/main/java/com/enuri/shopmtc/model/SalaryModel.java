package com.enuri.shopmtc.model;

import lombok.Getter;

@Getter
public class SalaryModel {
    private int id;
    private String name;
    private String email;

    //@NonNull @Builder.Default private String name="NULL NAME";
    //@NonNull @Builder.Default private String email="NULL EMAIL";
}
