package com.enuri.shopmtc.model;

import lombok.Getter;

@Getter
public class CountryModel {
    private int id;
    private String continent;
    private String country;
    private String catecd;
    private String crwlurl;

    //@NonNull @Builder.Default private String continent = "No CONTINENT";
    //@NonNull @Builder.Default private String country = "No Country";
}
