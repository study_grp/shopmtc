package com.enuri.shopmtc.mapper.slave1;

import java.util.List;
import com.enuri.shopmtc.model.CountryModel;

public interface Slave1DataBaseMapper {
    //public List<CountryModel> getCountry() throws Exception;

    public List<CountryModel> getCrwlReg() throws Exception;
}
