package com.enuri.shopmtc.mapper.master;
import java.util.List;
import com.enuri.shopmtc.model.SalaryModel;

public interface MasterDataBaseMapper {
    public List<SalaryModel> getSalary() throws Exception;
}
