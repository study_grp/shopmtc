package com.enuri.shopmtc.service;

import java.util.List;
import com.enuri.shopmtc.mapper.master.MasterDataBaseMapper;
import com.enuri.shopmtc.model.SalaryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MasterDataBaseService {
    @Autowired
    MasterDataBaseMapper masterDataBaseMapper;

    public List<SalaryModel> getSalary() throws Exception{
        return masterDataBaseMapper.getSalary();
    }

}
