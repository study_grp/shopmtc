package com.enuri.shopmtc.service;

import java.util.List;
import com.enuri.shopmtc.mapper.slave1.Slave1DataBaseMapper;
import com.enuri.shopmtc.model.CountryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Slave1DataBaseService {
    @Autowired
    Slave1DataBaseMapper slave1DataBaseMapper;

    //public List<CountryModel> getCountry() throws Exception{
    //    return slave1DataBaseMapper.getCountry();
    //}

    public List<CountryModel> getCrwlReg() throws Exception{
        return slave1DataBaseMapper.getCrwlReg();
    }
}
